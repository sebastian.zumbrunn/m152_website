package ninja.jfr.service;

import java.util.ArrayList;
import java.util.prefs.Preferences;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ninja.jfr.model.Media;
import ninja.jfr.model.Recipe;

@Service
public class MediaService {

	@Autowired
	private MediaRepository mediaRepository;
	
	public MediaService() {
	}
	
	public Media save(Media media) {
		System.out.println(media.getMimeType());
		System.out.println(media.getPath());
		System.out.println(media.getTitle());
		System.out.println(media.getId());
		return mediaRepository.save(media);
	}
	
	public Media getMedia(int id) {
		return mediaRepository.findById(id).orElse(null);
	}
	
	public void delete(int id) {
		mediaRepository.deleteById(id);
	}
	
	public ArrayList<Media> getAllMedias(){
		return (ArrayList<Media>) mediaRepository.findAll();
	}
	
	public void deleteAll() {
		mediaRepository.deleteAll();
	}
	
}
