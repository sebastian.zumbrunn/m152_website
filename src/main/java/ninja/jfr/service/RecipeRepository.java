package ninja.jfr.service;

import org.springframework.data.repository.CrudRepository;

import ninja.jfr.model.Recipe;

public interface RecipeRepository extends CrudRepository<Recipe, Integer>{

}
