package ninja.jfr.controller;

import java.util.*;

import ninja.jfr.model.Media;
import ninja.jfr.service.MediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import ninja.jfr.model.Recipe;
import ninja.jfr.service.RecipeService;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class RecipeController {
	
	private ArrayList<Recipe> recipeList;
	
	@Autowired
	private RecipeService recipeService;

	@GetMapping("/showRecipes")
	public String showJobs(Model model) {
		createRecipe();
		recipeList = recipeService.getAllRecipes();
		model.addAttribute("recipeList", recipeList);
		return "showRecipes";
	}

	@GetMapping("/addRecipe")
	public String addRecipe(Model model) {

		return "addRecipe";
	}

	public void createRecipe(){
		List<Media> medias = new ArrayList<>();
		medias.add(new Media("butterchicken", "/resources/static/img/butter_chicken.png"));
/*
		MediaService mediaService = new MediaService();
		mediaService.save(new Media("butterchicken", "/resources/static/img/butter_chicken.png"));
*/
		Recipe recipe = new Recipe("Butter Chicken", "Ein Indische Gericht.", "-Pouletschnitzel \n -Tandoori Paste", "kochen", null);
		 recipe = recipeService.save(recipe);
	}

}
